@extends('layouts.app')

@section('content')

   
    <!-- MAIN CONTENT -->
    <div id="main-content">
        
       

        <div id="content-div">
            <h1>How well can you play the higher and lower game?</h1>
            <div id="new-game">
                <a href="/game/new/cardkey/0/guess/higher">
                    <button type="button" class="btn margin-top-30 font-40 start-btn btn-green btn-lg">
                          Start a new game <span class="fa fa-play"></span>
                    </button>
                </a>
            </div>
           
           
        </div><!-- end of #content-div -->
    
    </div>{{-- End of #content --}}
    
@endsection

