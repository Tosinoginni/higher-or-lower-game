@extends('layouts.app')

@section('content')

   
        <!-- MAIN CONTENT -->
        <div id="main-content">
            
            
            @if(!$win)
                <h1>Guess what value is the next card, higher or lower ?</h1>
            @else
                <h1>You Won!</h1>
            @endif

            <div id="content-div">
                
                @if(!$win)
               
                    <div class="row">
                        
                        <div class="col-xs-6">
                            <div id="current-card"><p> </p>
                                <div class="hand">
                                    <div style="color:{!! $cardColor !!};" class="card  {!! $cardShape !!}">
                                        <p> {!! $currentCard['value'] !!} </p>
                                    </div>                                
                                </div>
                                <p></p>
                            </div>
                          
                        </div>

                        <div class="col-xs-6">
                            <div id="higher-lower">
                                <a href="/game/new/cardkey/{{$nextKey}}/guess/higher">
                                    <button class="higher-button"><i class="guess-btn fa fa-chevron-up"></i></button>
                                </a>
                                <br>
                                <a href="/game/new/cardkey/{{$nextKey}}/guess/lower">
                                    <button class="lower-button"><i class="guess-btn fa fa-chevron-down"></i></button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <?php
                                $livesText = "lives";
                                if($lives == 1)
                                {
                                    $livesText = "life";
                                }
                            ?>
                           <p style="float:right">You have {{$lives}} {{$livesText}} to go</p>
                        </div>
                        <div class="col-xs-6">
                            <div id="lives">
                                <?php for($i=1; $i<=$lives;  $i++){ ?>
                                    <i class="lives fa fa-heart"></i>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-xs-8 col-xs-offset-2">
                        <div>
                        
                            <img src="/img/fireworks.gif">
                        </div>
                    
                        <div id="new-game">
                            <a href="/game/new/cardkey/0/guess/higher">
                                <button type="button" class="btn margin-top-30 font-40 start-btn btn-green btn-lg">
                                      Play Again <span class="fa fa-play"></span>
                                </button>
                            </a>
                        </div>
                    </div>
                @endif
               
            </div><!-- end of #content-div -->
        
        </div>{{-- End of #content --}}
    
@endsection

