@extends('layouts.app')

@section('content')

   
        <!-- MAIN CONTENT -->
        <div id="main-content">
            
            <h1>Game Over</h1>

            <div id="content-div">
                
               
                    <div class="row">
                        
                        <div class="col-xs-6">
                            <div id="current-card"><p> </p>
                                <div class="hand">
                                    <div style="color:{!! $cardColor !!};" class="card  {!! $cardShape !!}">
                                        <p> {!! $failedcard['value'] !!} </p>
                                    </div>                                
                                </div>
                                <p></p>
                            </div>
                          
                        </div>

                        <div class="col-xs-6">
                            <div id="higher-lower">
                                <p class="game-over">Game over!</p>
                                <p class="your-score">You scored</p>
                                <p class="score">{{$score}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div id="new-game">
                            <a href="/game/new/cardkey/0/guess/higher">
                                <button type="button" class="btn margin-top-30 font-40 start-btn btn-green btn-lg">
                                      Start a new game <span class="fa fa-play"></span>
                                </button>
                            </a>
                        </div>
                    </div>
                    
                    
               
            </div><!-- end of #content-div -->
        
        </div>{{-- End of #content --}}
    
@endsection

