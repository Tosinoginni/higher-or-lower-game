<?php





Route::get('/game/welcome', 							array("as" => "new_game", 	"uses"  => "PlayGameController@welcome" ));
Route::get('/game/over/cardkey/{cardkey}', 				array("as" => "game_over",  "uses"  => "PlayGameController@gameOver" ));
Route::get('/game/new/cardkey/{key}/guess/{level}', 	array("as" => "guess",  	"uses"  => "PlayGameController@newGame" ));


