
## To start the game 
Once you start up your server click
http://localhost/game/welcome


## Procedure:

- Once user click on start game, call API and check if response if OK
- Convert the return result to an array and put in a session to avoid calling the API multiple times
- Shuffle the card and render it the player
- Analyse player's guesses and return message in respect to that
- once game is over, clear all sessions
- If player wins, clear all sessions
- If player click on play again, start this whole scenario again

## Game Window
-![startgame](public/img/start-window.png)

-![startgame](public/img/playing-window.png)

-![startgame](public/img/game-over-window.png)

-![startgame](public/img/win-window.png)