<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Routing\Route;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Controllers\Support\RequestCardApi;
use App\Http\Controllers\Support\card;
use App\Http\Controllers\Support\Life;
use App\Http\Controllers\Support\Game;


class PlayGameController extends Controller
{
    
	use RequestCardApi, Card, Life, Game;



    public function welcome(Request $request)
    {
        $this->resetGame($request);
    	return view("game.index");
    }


    /**
	* @return view with required parameter
    */
    public function gameOver(Request $request, $cardkey)
    {
    	$score = 0;
    	$cardColor = "";
        
        if($cardkey)
        {
            $getCards = $this->newCards($request);
            
            $score = ($cardkey - 1); 
            $failedcard = $getCards[$score];
        
            $cardColor  = $this->getCurrentCardColor($failedcard);
            $cardShape  = "suit" . $failedcard['suit'];
        }

        $this->resetGame($request);
        return view("game.over", compact("failedcard", "cardColor", "cardShape", "score"));

    }

   
    /**
    *@return view for new game
    */
    public function newGame(Request $request, $cardkey, $guess)
    {
    	$cards = $this->newCards($request);
    	
        // If Game is over
    	$gameover = $this->isGameOver($request);
    	if($gameover)
    	{
            return redirect()->route("game_over", $cardkey);	 	
    	}

    	
    	if($cardkey < 52)
    	{
    		
    		$currentCardValue 	= $this->getCardValue($cards, $cardkey);
    		if($cardkey<=0)
    		{
    			$prevCardValue = $currentCardValue;
    		}
    		else
            {
    			$prevCardValue 	= $this->getCardValue($cards, $cardkey - 1);
    		}
    		
            $this->updatePlayersLives($request, $guess, $prevCardValue, $currentCardValue);

    		$currentCard = $cards[$cardkey];
            $nextKey 	= $cardkey + 1;
    		$cardColor 	= $this->getCurrentCardColor($currentCard);
    		$cardShape 	= "suit" . $currentCard['suit'];
    		$lives 		= $this->getLives($request);
        }

        $win = $this->win($request, $cardkey);    	
    	return view("game.new", compact("currentCard", "cardColor", "cardShape", "nextKey", "lives", "gameover", "win"));
    	
    }


    /**
	* @return All session destroy
    */
    public function resetGame($request)
    {
    	return $request->session()->flush();
    }

}
