<?php
namespace App\Http\Controllers\Support; 


trait Game{

    protected $gameOver = false;


    /**
    *@return bool
    */
    public function win($request, $cardkey)
    {
        if(!$this->isGameOver($request))
        {
            if($cardkey == 51)
            {
                return true;
            }
        }
        return false;
    }

    

    /**
    * Check if game is over
    *@return bool
    */
    private function isGameOver($request)
    {
        if($request->session()->exists('lives')) 
        {
            $remaningLives = $request->session()->get('lives');

            if($remaningLives <= 1)
            {
                // Redirect and reset 
                $this->gameOver = true;
            }
        }
        return $this->gameOver;
    }




}

?>