<?php
namespace App\Http\Controllers\Support; 


trait Life{

    
	protected $lives = 3;
    

    /**
    * Update players lives based on guesses
    */
    protected function updatePlayersLives($request, $guess, $prevCardValue, $currentCardValue)
    {
        if($guess == "higher")
        {
            if($prevCardValue > $currentCardValue)
            {
                return $this->updateLives($request, - 1);
            }
            
        }
        elseif($guess == "lower")
        {
            if($prevCardValue < $currentCardValue)
            {
                return $this->updateLives($request, - 1);
            }
            
        }
    }

    /**
    * Update lives number of live 
    */
	protected function updateLives($request, $updatedValue)
    {
        if($request->session()->exists('lives')) 
        {
            $old_lives = $request->session()->get('lives');
            $lives = ($old_lives + $updatedValue );
            $request->session()->put('lives', $lives);
        }
        else
        {
            $lives = ($this->lives + $updatedValue);
            $request->session()->put('lives', $lives);
        }

        return $lives ;
    }


    /**
    *@return number lives
    */
    public function getLives($request)
    {
        if($request->session()->exists('lives')) 
        {
            return  $request->session()->get('lives');
        }

        return $this->lives;
    }


}

?>