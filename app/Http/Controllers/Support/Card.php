<?php
namespace App\Http\Controllers\Support; 


trait Card{

	
	/**
	*@return card value
    */
    public function getCardValue($cards, $key)
    {
    	$value = $cards[$key]["value"];

    	if($value == "A"){$value = 1;}
    	if($value == "J"){$value = 11;}
    	if($value == "Q"){$value = 12;}
    	if($value == "K"){$value = 13;}

    	return $value;
    }


    
    /**
	*@return array if cards
    */
    public function newCards($request)
    {

    	if(! $request->session()->exists('allCards')) 
    	{
    		$allCardInArray = $this->requestAllCards($request);

    		shuffle($allCardInArray);
    		
    		$this->putArrayCardsInSession($request, $allCardInArray);    		
    	}
    	else
    	{
    		$allCardInArray = $request->session()->get('allCards');    	
    	}
    	return $allCardInArray;       
    }


    /**
	* Get Card from API and store in a session
	* 
    */
    public function requestAllCards($request)
    {
    		
    	if($request->session()->exists('cardInArray')) 
    	{
    		 $cards = $request->session()->get('cardInArray');
    	}
    	else
    	{
    		$cards =  json_decode($this->requestCardsAPi(), true);
    		$request->session()->put('cardInArray', $cards);
    	}

    	return $cards;
    }


    public function putArrayCardsInSession($request, $cardSet)
    {
    	return $request->session()->put('allCards', $cardSet);
    }

    
    /**
    *@return card color
    */
    public function getCurrentCardColor($currentCard)
    {
        $shape = $currentCard['suit'];

        if(($shape  == "diamonds") || ( $shape == "hearts")   )
        {
            $color = "red";
        }
        elseif( ($shape == "spades") ||( $shape == "clubs") )
        {
            $color = "black";
        }

        return $color;
    }




}

?>