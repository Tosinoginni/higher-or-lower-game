<?php
namespace App\Http\Controllers\Support; 

use GuzzleHttp\Client;


trait RequestCardApi{

	protected $cardAPI = "https://cards.davidneal.io/api/cards";

	public function requestCardsAPi(){
	
		//create an instance of Client wiht base url of the API
		$client = new Client();

		try{
			$response = $client->request('GET', $this->cardAPI, []);

			// check the response by
			$response;

			// Get the response status code
			$response->getStatusCode();

			// Get the response phase
			$response->getReasonPhrase();

			// Get all/full header
			$response->getHeaders();

			// Get specific specific entity of header, 
			// here we retrive content-type
			$response->getHeader('content-type');
		}
		catch(\Exception $ex)
		{
			return back()->withError($ex->getMessage());
		}

		// Get api content. it return the main content the we need

		return $response->getBody()->getContents();
	}

}

?>